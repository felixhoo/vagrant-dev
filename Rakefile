require 'yaml'
require 'digest/md5'
require 'terminal-color'

FILES_PATH = File.join(File.dirname(__FILE__), 'files')

def file_checksum(f)
  Digest::MD5.file(f).hexdigest
end

desc "Download blobs according to files.yml"
task :files do
  files = YAML.load_file("#{FILES_PATH}.yml")

  def download_blob(name, url)
    Dir.chdir(FILES_PATH) { sh "wget #{url} -O #{name}" }
  end

  def blob_exists?(name, exp_md5)
    file = File.join(FILES_PATH, name)
    File.exists?(file) && file_checksum(file) == exp_md5
  end

  FileUtils.mkdir_p(FILES_PATH)
  files.each do |name, info|
    download_blob(name, info["url"]) unless blob_exists?(name, info["md5"])
  end
  puts "Files are ready".green
end

task :default => :files
